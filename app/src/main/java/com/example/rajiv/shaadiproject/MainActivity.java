package com.example.rajiv.shaadiproject;

import android.arch.lifecycle.Observer;
import android.arch.paging.PagedList;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.example.rajiv.shaadiproject.api.WebServices;
import com.example.rajiv.shaadiproject.data.ResultDataSource;
import com.example.rajiv.shaadiproject.model.Matches;
import com.example.rajiv.shaadiproject.model.RequestFailure;
import com.example.rajiv.shaadiproject.util.ApiClient;
import com.example.rajiv.shaadiproject.util.MainThreadExecutor;
import com.example.rajiv.shaadiproject.util.RecyclerviewDragDrop.MyItemTouchHelperCallback;

public class MainActivity extends AppCompatActivity {


    private MainThreadExecutor executor;
    private MatchesAdapter matchesAdapter;
    private WebServices service;
    private ItemTouchHelper itemTouchHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        executor = new MainThreadExecutor();
        matchesAdapter = new MatchesAdapter(this);

        setupGitHubService();
        setupDataSource();
        setupRecyclerView();

    }


    private void setupRecyclerView() {

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(matchesAdapter);

        ItemTouchHelper.Callback callback = new MyItemTouchHelperCallback(matchesAdapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    private void setupGitHubService() {
        service = ApiClient.getClient(this).create(WebServices.class);
    }

    private void setupDataSource() {

        ResultDataSource dataSource = new ResultDataSource(service);

        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(WebServices.DEFAULT_PER_PAGE)
                .setInitialLoadSizeHint(WebServices.DEFAULT_PER_PAGE * 2)
                .setEnablePlaceholders(true)
                .build();

        PagedList<Matches.ResultsBean> list =
                new PagedList.Builder<>(dataSource, config)
                        .setFetchExecutor(executor)
                        .setNotifyExecutor(executor)
                        .build();


        matchesAdapter.submitList(list);

        dataSource.getRequestFailureLiveData().observe(this, new Observer<RequestFailure>() {
            @Override
            public void onChanged(@Nullable final RequestFailure requestFailure) {
                if (requestFailure == null) return;

                Snackbar.make(findViewById(android.R.id.content), requestFailure.getErrorMessage(), Snackbar.LENGTH_INDEFINITE)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestFailure.getRetryable().retry();
                            }
                        }).show();
            }
        });
    }
}


