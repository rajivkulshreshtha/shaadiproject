package com.example.rajiv.shaadiproject;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.example.rajiv.shaadiproject.model.Matches;
import com.example.rajiv.shaadiproject.util.RecyclerviewDragDrop.ItemTouchHelperAdapter;


public class MatchesAdapter extends PagedListAdapter<Matches.ResultsBean, MatchesViewHolder> implements ItemTouchHelperAdapter {

    Context context;

    MatchesAdapter(Context context) {
        super(Matches.ResultsBean.DIFF_CALLBACK);
        this.context = context;

    }

    @NonNull
    @Override
    public MatchesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return MatchesViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchesViewHolder holder, int position) {
        holder.bindTo(context, getItem(position));
    }

    @Override
    public void onItemDismiss(int position) {

       try{
           getCurrentList().remove(position);
           notifyItemRemoved(position);
       }catch (Exception e){

       }
    }
}