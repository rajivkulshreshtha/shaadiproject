package com.example.rajiv.shaadiproject.util;

import android.content.Context;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BASE_URL = " https://randomuser.me/";
    private static Retrofit retrofit = null;
    private static OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

    static {

        if (AppConstant.DEBUG) {
            okHttpBuilder.addInterceptor(loggingInterceptor);
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

    }

    public static Retrofit getClient(final Context context) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpBuilder.build())
                    .build();
        }
        return retrofit;
    }

}

