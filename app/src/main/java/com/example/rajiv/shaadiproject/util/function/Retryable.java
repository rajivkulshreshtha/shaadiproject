package com.example.rajiv.shaadiproject.util.function;

public interface Retryable {

    void retry();
}
