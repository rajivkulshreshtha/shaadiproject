package com.example.rajiv.shaadiproject.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.example.rajiv.shaadiproject.model.Matches;
import com.example.rajiv.shaadiproject.model.RequestFailure;
import com.example.rajiv.shaadiproject.api.WebServices;
import com.example.rajiv.shaadiproject.util.function.Retryable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

public class ResultDataSource extends PageKeyedDataSource<Integer, Matches.ResultsBean> {

    private final WebServices service;
    private final MutableLiveData<RequestFailure> requestFailureLiveData;

    public ResultDataSource(WebServices service) {
        this.service = service;
        this.requestFailureLiveData = new MutableLiveData<>();
    }

    @Override
    public void loadInitial(@NonNull final LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Matches.ResultsBean> callback) {

        final int page = 1;

        Call<Matches> call = service.getMatches(String.valueOf(params.requestedLoadSize));

        Callback<Matches> requestCallback = new Callback<Matches>() {
            @Override
            public void onResponse(@NonNull Call<Matches> call, @NonNull Response<Matches> response) {
                Matches matches = response.body();

                if (matches == null) {
                    onFailure(call, new HttpException(response));
                    return;
                }

                callback.onResult(
                        matches.getResults(),
                        0,
                        matches.getTotalCount(),
                        null,
                        page + 1
                );
            }

            @Override
            public void onFailure(@NonNull Call<Matches> call, @NonNull Throwable t) {
                Retryable retryable = new Retryable() {
                    @Override
                    public void retry() {
                        loadInitial(params, callback);
                    }
                };

                handleError(retryable, t);
            }
        };

        call.enqueue(requestCallback);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Matches.ResultsBean> callback) {
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Matches.ResultsBean> callback) {

        final int page = params.key;
        Call<Matches> call = service.getMatches(String.valueOf(params.requestedLoadSize));
        Callback<Matches> requestCallback = new Callback<Matches>() {
            @Override
            public void onResponse(@NonNull Call<Matches> call, @NonNull Response<Matches> response) {
                Matches matches = response.body();

                if (matches == null) {
                    onFailure(call, new HttpException(response));
                    return;
                }

                callback.onResult(
                        matches.getResults(),
                        page + 1
                );
            }

            @Override
            public void onFailure(@NonNull Call<Matches> call, @NonNull Throwable t) {
                Retryable retryable = new Retryable() {
                    @Override
                    public void retry() {
                        loadAfter(params, callback);
                    }
                };

                handleError(retryable, t);
            }
        };

        call.enqueue(requestCallback);
    }

    public LiveData<RequestFailure> getRequestFailureLiveData() {
        return requestFailureLiveData;
    }

    private void handleError(Retryable retryable, Throwable t) {
        requestFailureLiveData.postValue(new RequestFailure(retryable, t.getMessage()));
    }

}
