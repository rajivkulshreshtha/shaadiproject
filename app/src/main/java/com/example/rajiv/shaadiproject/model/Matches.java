package com.example.rajiv.shaadiproject.model;

import android.support.v7.util.DiffUtil;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Matches {


    /**
     * results : [{"gender":"male","name":{"title":"mr","first":"dean","last":"perkins"},"location":{"street":"9250 church lane","city":"leixlip","state":"laois","postcode":40134,"coordinates":{"latitude":"19.2710","longitude":"-93.1612"},"timezone":{"offset":"+4:30","description":"Kabul"}},"email":"dean.perkins@example.com","login":{"uuid":"3bae0875-6f72-4e44-b136-2852d9f31a39","username":"bluegorilla254","password":"barfly","salt":"z9Dk4qrl","md5":"0d1b273c782b6aa2f690f28e242a276d","sha1":"55a7a97989d8e4820aa103d329222fcf6e6d3d59","sha256":"d9a70dfe870c1d53388e02fe52a4ab7a0ec6878f97899487bb7fbb0e73733242"},"dob":{"date":"1972-03-25T12:06:47Z","age":46},"registered":{"date":"2007-08-10T20:49:46Z","age":11},"phone":"071-692-0626","cell":"081-207-8706","id":{"name":"PPS","value":"6377630T"},"picture":{"large":"https://randomuser.me/api/portraits/men/13.jpg","medium":"https://randomuser.me/api/portraits/med/men/13.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/13.jpg"},"nat":"IE"},{"gender":"male","name":{"title":"mr","first":"kirk","last":"palmer"},"location":{"street":"767 o'connell avenue","city":"ennis","state":"monaghan","postcode":44949,"coordinates":{"latitude":"-37.8968","longitude":"-168.1426"},"timezone":{"offset":"+11:00","description":"Magadan, Solomon Islands, New Caledonia"}},"email":"kirk.palmer@example.com","login":{"uuid":"bdc1aa5b-b950-4949-bf18-6a667344df69","username":"yellowmouse914","password":"malibu","salt":"AEmsq19A","md5":"560508baa95c4ac787338cd17b8de820","sha1":"d4a8a3567809e5ad32ef4e2f7f53f4617f703f44","sha256":"5f47969025b7ffffece9153d790e1db7d82893561edc834c65f74702db3269ce"},"dob":{"date":"1985-12-21T14:22:06Z","age":32},"registered":{"date":"2005-07-15T16:14:44Z","age":13},"phone":"031-426-2980","cell":"081-625-3775","id":{"name":"PPS","value":"0281132T"},"picture":{"large":"https://randomuser.me/api/portraits/men/90.jpg","medium":"https://randomuser.me/api/portraits/med/men/90.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/90.jpg"},"nat":"IE"},{"gender":"female","name":{"title":"ms","first":"andréa","last":"lecomte"},"location":{"street":"8822 place de l'église","city":"poitiers","state":"val-d'oise","postcode":99749,"coordinates":{"latitude":"84.5232","longitude":"44.4117"},"timezone":{"offset":"-10:00","description":"Hawaii"}},"email":"andréa.lecomte@example.com","login":{"uuid":"ec3e4639-4ed0-4a39-9bb8-04c22548e18f","username":"ticklishleopard126","password":"lakewood","salt":"I2wCOln4","md5":"c36572a9f9d5c019edf86a59a812b28a","sha1":"6300fcdbbbacb1ba32c81779e2102c41c2caad7c","sha256":"b4482e089af17a513ac7367d1949268184d482017cf83c90ffea740202035ea4"},"dob":{"date":"1986-07-24T05:19:09Z","age":32},"registered":{"date":"2005-08-18T16:16:46Z","age":13},"phone":"03-71-42-75-63","cell":"06-42-46-43-83","id":{"name":"INSEE","value":"2NNaN13090750 87"},"picture":{"large":"https://randomuser.me/api/portraits/women/29.jpg","medium":"https://randomuser.me/api/portraits/med/women/29.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/women/29.jpg"},"nat":"FR"},{"gender":"male","name":{"title":"mr","first":"lawrence","last":"otoole"},"location":{"street":"9609 new street","city":"clonakilty","state":"mayo","postcode":50738,"coordinates":{"latitude":"-74.5446","longitude":"137.6160"},"timezone":{"offset":"+3:00","description":"Baghdad, Riyadh, Moscow, St. Petersburg"}},"email":"lawrence.otoole@example.com","login":{"uuid":"3bef8d57-a33e-48d8-8c34-9b263c895737","username":"purpleswan851","password":"656565","salt":"neTbGvKM","md5":"e0b8774a482e156dd5dc4e371ad1c6eb","sha1":"25a8cb0ab4d93fe8d20857651d24c608a0494b9d","sha256":"ae863a1bccf6c095d175c037f152d6c6d410284de876de65b48358528160f547"},"dob":{"date":"1974-03-08T01:41:05Z","age":44},"registered":{"date":"2011-08-12T21:32:54Z","age":7},"phone":"031-711-7061","cell":"081-777-4688","id":{"name":"PPS","value":"8706306T"},"picture":{"large":"https://randomuser.me/api/portraits/men/5.jpg","medium":"https://randomuser.me/api/portraits/med/men/5.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/5.jpg"},"nat":"IE"},{"gender":"female","name":{"title":"ms","first":"leanne","last":"ennis"},"location":{"street":"4172 dieppe ave","city":"inverness","state":"ontario","postcode":"T7O 8T6","coordinates":{"latitude":"35.3232","longitude":"-3.3968"},"timezone":{"offset":"-10:00","description":"Hawaii"}},"email":"leanne.ennis@example.com","login":{"uuid":"5d5251e0-b90d-4fa6-9b8f-74a731b08599","username":"lazytiger210","password":"lion","salt":"qCyoDPRt","md5":"4e32b4085adbfb603108d825c79b8499","sha1":"747c2a74c7c3e9d4b6a359c8af8a751a51dbe1c5","sha256":"d27323b865a2c3e5b155349241ffad83c5b83514d0d87afd5377cab18ad3acc7"},"dob":{"date":"1944-09-30T11:48:18Z","age":73},"registered":{"date":"2007-06-08T10:21:36Z","age":11},"phone":"358-277-5212","cell":"592-428-2664","id":{"name":"","value":null},"picture":{"large":"https://randomuser.me/api/portraits/women/6.jpg","medium":"https://randomuser.me/api/portraits/med/women/6.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/women/6.jpg"},"nat":"CA"},{"gender":"female","name":{"title":"ms","first":"liva","last":"norderhaug"},"location":{"street":"furubråtveien 8193","city":"alvdal","state":"buskerud","postcode":"0215","coordinates":{"latitude":"-80.5510","longitude":"48.5825"},"timezone":{"offset":"+6:00","description":"Almaty, Dhaka, Colombo"}},"email":"liva.norderhaug@example.com","login":{"uuid":"dd8905b2-2c1a-42bc-b79d-eec0b60a29a4","username":"angrycat757","password":"hornet","salt":"b6wuQsli","md5":"d6d2ef5ee6d3a534e77095a554be1848","sha1":"23e2471a7cfc9370f6aab66320363434735f4290","sha256":"6dfc119a48b486455c376d392ef862e60e3033ce1ac0068701436fec72f0a9b4"},"dob":{"date":"1988-05-30T06:46:41Z","age":30},"registered":{"date":"2003-01-18T04:57:19Z","age":15},"phone":"77506397","cell":"90702097","id":{"name":"FN","value":"30058802995"},"picture":{"large":"https://randomuser.me/api/portraits/women/81.jpg","medium":"https://randomuser.me/api/portraits/med/women/81.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/women/81.jpg"},"nat":"NO"},{"gender":"male","name":{"title":"mr","first":"ryan","last":"henry"},"location":{"street":"9341 plum st","city":"san jose","state":"pennsylvania","postcode":22089,"coordinates":{"latitude":"83.1202","longitude":"-147.1104"},"timezone":{"offset":"0:00","description":"Western Europe Time, London, Lisbon, Casablanca"}},"email":"ryan.henry@example.com","login":{"uuid":"657a4476-37ab-4588-a105-158c4cc94249","username":"tinyfish136","password":"1988","salt":"i0swGkLW","md5":"d5914438434e723ca12dd5c719a352e3","sha1":"0b9cc664fb9590d4ac5347b6896cbba335811f56","sha256":"daf0e1583bb59886686674e60a68aa6d9118c43dc1ade475d12004a8cdc340ad"},"dob":{"date":"1979-07-03T08:35:55Z","age":39},"registered":{"date":"2005-04-17T19:34:50Z","age":13},"phone":"(918)-791-6377","cell":"(657)-257-6193","id":{"name":"SSN","value":"349-61-6371"},"picture":{"large":"https://randomuser.me/api/portraits/men/10.jpg","medium":"https://randomuser.me/api/portraits/med/men/10.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/10.jpg"},"nat":"US"},{"gender":"female","name":{"title":"ms","first":"sandra","last":"lucas"},"location":{"street":"4969 rue de gerland","city":"nantes","state":"loire-atlantique","postcode":85763,"coordinates":{"latitude":"74.1168","longitude":"-106.5040"},"timezone":{"offset":"+10:00","description":"Eastern Australia, Guam, Vladivostok"}},"email":"sandra.lucas@example.com","login":{"uuid":"64d3ba7c-c8db-4bcc-99dc-830270e55a42","username":"redleopard365","password":"deeper","salt":"AwIm8eGY","md5":"a6789c558d36f44ec8eae3f4e578d3b9","sha1":"49809437b261fc639b82c17c2333e3c201282140","sha256":"d982345d80ce8d2ceaafd9b2806fd916ed035740482d930bdc9c8d1926b9f396"},"dob":{"date":"1984-05-21T20:33:51Z","age":34},"registered":{"date":"2007-04-11T03:34:06Z","age":11},"phone":"02-98-79-50-42","cell":"06-90-23-38-90","id":{"name":"INSEE","value":"2NNaN24237068 65"},"picture":{"large":"https://randomuser.me/api/portraits/women/74.jpg","medium":"https://randomuser.me/api/portraits/med/women/74.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/women/74.jpg"},"nat":"FR"},{"gender":"male","name":{"title":"mr","first":"gabriel","last":"soto"},"location":{"street":"5018 avenida de castilla","city":"parla","state":"castilla la mancha","postcode":21555,"coordinates":{"latitude":"-65.3294","longitude":"-18.8188"},"timezone":{"offset":"-11:00","description":"Midway Island, Samoa"}},"email":"gabriel.soto@example.com","login":{"uuid":"02bd0ddb-90b5-4eba-9c04-0ebfd0f6d67e","username":"whiteladybug163","password":"ernesto","salt":"P6B2L4yE","md5":"ed83329ca2a31a4575f013c767188612","sha1":"eab4cbc2ae4e95db77076f2c096cfc9d1b0b63a7","sha256":"0c23cda997af5f2a31fb8a8cc6238da97d72ad0c6e444eed0131d7dfdecd3b5f"},"dob":{"date":"1958-05-24T14:39:44Z","age":60},"registered":{"date":"2003-10-26T23:15:39Z","age":14},"phone":"967-009-949","cell":"699-436-973","id":{"name":"DNI","value":"38489199-I"},"picture":{"large":"https://randomuser.me/api/portraits/men/51.jpg","medium":"https://randomuser.me/api/portraits/med/men/51.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/51.jpg"},"nat":"ES"},{"gender":"male","name":{"title":"mr","first":"benjamin","last":"poulsen"},"location":{"street":"7930 runddyssen","city":"askeby","state":"hovedstaden","postcode":81196,"coordinates":{"latitude":"23.3214","longitude":"105.3582"},"timezone":{"offset":"-8:00","description":"Pacific Time (US & Canada)"}},"email":"benjamin.poulsen@example.com","login":{"uuid":"c22f4582-9c0e-440a-b1f2-dcf295bba9b3","username":"organictiger510","password":"looney","salt":"GdgqYsmz","md5":"e5d7a09223a76e484f334d571eb092ed","sha1":"b7d203070e9714248849ea831c80f01808d82df7","sha256":"688d287571dd519a55cb8d866312faf2a0b90fe4d4db3a01950eec742f637e71"},"dob":{"date":"1973-03-01T17:36:07Z","age":45},"registered":{"date":"2010-06-09T22:39:23Z","age":8},"phone":"09866709","cell":"79947902","id":{"name":"CPR","value":"192673-2324"},"picture":{"large":"https://randomuser.me/api/portraits/men/25.jpg","medium":"https://randomuser.me/api/portraits/med/men/25.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/25.jpg"},"nat":"DK"}]
     * info : {"seed":"05f78b550b75ff0b","results":10,"page":1,"version":"1.2"}
     */


    @SerializedName("info")
    private InfoBean info;
    @SerializedName("results")
    private List<ResultsBean> results;

    private Integer totalCount = 1000;


    public Integer getTotalCount() {
        return totalCount;
    }

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class InfoBean {
        /**
         * seed : 05f78b550b75ff0b
         * results : 10
         * page : 1
         * version : 1.2
         */

        @SerializedName("seed")
        private String seed;
        @SerializedName("results")
        private int results;
        @SerializedName("page")
        private int page;
        @SerializedName("version")
        private String version;

        public String getSeed() {
            return seed;
        }

        public void setSeed(String seed) {
            this.seed = seed;
        }

        public int getResults() {
            return results;
        }

        public void setResults(int results) {
            this.results = results;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }

    public static class ResultsBean {
        /**
         * gender : male
         * name : {"title":"mr","first":"dean","last":"perkins"}
         * location : {"street":"9250 church lane","city":"leixlip","state":"laois","postcode":40134,"coordinates":{"latitude":"19.2710","longitude":"-93.1612"},"timezone":{"offset":"+4:30","description":"Kabul"}}
         * email : dean.perkins@example.com
         * login : {"uuid":"3bae0875-6f72-4e44-b136-2852d9f31a39","username":"bluegorilla254","password":"barfly","salt":"z9Dk4qrl","md5":"0d1b273c782b6aa2f690f28e242a276d","sha1":"55a7a97989d8e4820aa103d329222fcf6e6d3d59","sha256":"d9a70dfe870c1d53388e02fe52a4ab7a0ec6878f97899487bb7fbb0e73733242"}
         * dob : {"date":"1972-03-25T12:06:47Z","age":46}
         * registered : {"date":"2007-08-10T20:49:46Z","age":11}
         * phone : 071-692-0626
         * cell : 081-207-8706
         * id : {"name":"PPS","value":"6377630T"}
         * picture : {"large":"https://randomuser.me/api/portraits/men/13.jpg","medium":"https://randomuser.me/api/portraits/med/men/13.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/13.jpg"}
         * nat : IE
         */

        @SerializedName("gender")
        private String gender;
        @SerializedName("name")
        private NameBean name;
        @SerializedName("location")
        private LocationBean location;
        @SerializedName("email")
        private String email;
        @SerializedName("login")
        private LoginBean login;
        @SerializedName("dob")
        private DobBean dob;
        @SerializedName("registered")
        private RegisteredBean registered;
        @SerializedName("phone")
        private String phone;
        @SerializedName("cell")
        private String cell;
        @SerializedName("id")
        private IdBean id;
        @SerializedName("picture")
        private PictureBean picture;
        @SerializedName("nat")
        private String nat;

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public NameBean getName() {
            return name;
        }

        public void setName(NameBean name) {
            this.name = name;
        }

        public LocationBean getLocation() {
            return location;
        }

        public void setLocation(LocationBean location) {
            this.location = location;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public LoginBean getLogin() {
            return login;
        }

        public void setLogin(LoginBean login) {
            this.login = login;
        }

        public DobBean getDob() {
            return dob;
        }

        public void setDob(DobBean dob) {
            this.dob = dob;
        }

        public RegisteredBean getRegistered() {
            return registered;
        }

        public void setRegistered(RegisteredBean registered) {
            this.registered = registered;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCell() {
            return cell;
        }

        public void setCell(String cell) {
            this.cell = cell;
        }

        public IdBean getId() {
            return id;
        }

        public void setId(IdBean id) {
            this.id = id;
        }

        public PictureBean getPicture() {
            return picture;
        }

        public void setPicture(PictureBean picture) {
            this.picture = picture;
        }

        public String getNat() {
            return nat;
        }

        public void setNat(String nat) {
            this.nat = nat;
        }

        public static class NameBean {
            /**
             * title : mr
             * first : dean
             * last : perkins
             */

            @SerializedName("title")
            private String title;
            @SerializedName("first")
            private String first;
            @SerializedName("last")
            private String last;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getFirst() {
                return first;
            }

            public void setFirst(String first) {
                this.first = first;
            }

            public String getLast() {
                return last;
            }

            public void setLast(String last) {
                this.last = last;
            }
        }

        public static class LocationBean {
            /**
             * street : 9250 church lane
             * city : leixlip
             * state : laois
             * postcode : 40134
             * coordinates : {"latitude":"19.2710","longitude":"-93.1612"}
             * timezone : {"offset":"+4:30","description":"Kabul"}
             */

            @SerializedName("street")
            private String street;
            @SerializedName("city")
            private String city;
            @SerializedName("state")
            private String state;
            @SerializedName("postcode")
            private String postcode;
            @SerializedName("coordinates")
            private CoordinatesBean coordinates;
            @SerializedName("timezone")
            private TimezoneBean timezone;

            public String getStreet() {
                return street;
            }

            public void setStreet(String street) {
                this.street = street;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getPostcode() {
                return postcode;
            }

            public void setPostcode(String postcode) {
                this.postcode = postcode;
            }

            public CoordinatesBean getCoordinates() {
                return coordinates;
            }

            public void setCoordinates(CoordinatesBean coordinates) {
                this.coordinates = coordinates;
            }

            public TimezoneBean getTimezone() {
                return timezone;
            }

            public void setTimezone(TimezoneBean timezone) {
                this.timezone = timezone;
            }

            public static class CoordinatesBean {
                /**
                 * latitude : 19.2710
                 * longitude : -93.1612
                 */

                @SerializedName("latitude")
                private String latitude;
                @SerializedName("longitude")
                private String longitude;

                public String getLatitude() {
                    return latitude;
                }

                public void setLatitude(String latitude) {
                    this.latitude = latitude;
                }

                public String getLongitude() {
                    return longitude;
                }

                public void setLongitude(String longitude) {
                    this.longitude = longitude;
                }
            }

            public static class TimezoneBean {
                /**
                 * offset : +4:30
                 * description : Kabul
                 */

                @SerializedName("offset")
                private String offset;
                @SerializedName("description")
                private String description;

                public String getOffset() {
                    return offset;
                }

                public void setOffset(String offset) {
                    this.offset = offset;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }
            }
        }

        public static class LoginBean {
            /**
             * uuid : 3bae0875-6f72-4e44-b136-2852d9f31a39
             * username : bluegorilla254
             * password : barfly
             * salt : z9Dk4qrl
             * md5 : 0d1b273c782b6aa2f690f28e242a276d
             * sha1 : 55a7a97989d8e4820aa103d329222fcf6e6d3d59
             * sha256 : d9a70dfe870c1d53388e02fe52a4ab7a0ec6878f97899487bb7fbb0e73733242
             */

            @SerializedName("uuid")
            private String uuid;
            @SerializedName("username")
            private String username;
            @SerializedName("password")
            private String password;
            @SerializedName("salt")
            private String salt;
            @SerializedName("md5")
            private String md5;
            @SerializedName("sha1")
            private String sha1;
            @SerializedName("sha256")
            private String sha256;

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getSalt() {
                return salt;
            }

            public void setSalt(String salt) {
                this.salt = salt;
            }

            public String getMd5() {
                return md5;
            }

            public void setMd5(String md5) {
                this.md5 = md5;
            }

            public String getSha1() {
                return sha1;
            }

            public void setSha1(String sha1) {
                this.sha1 = sha1;
            }

            public String getSha256() {
                return sha256;
            }

            public void setSha256(String sha256) {
                this.sha256 = sha256;
            }
        }

        public static class DobBean {
            /**
             * date : 1972-03-25T12:06:47Z
             * age : 46
             */

            @SerializedName("date")
            private String date;
            @SerializedName("age")
            private int age;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }
        }

        public static class RegisteredBean {
            /**
             * date : 2007-08-10T20:49:46Z
             * age : 11
             */

            @SerializedName("date")
            private String date;
            @SerializedName("age")
            private int age;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }
        }

        public static class IdBean {
            /**
             * name : PPS
             * value : 6377630T
             */

            @SerializedName("name")
            private String name;
            @SerializedName("value")
            private String value;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public static class PictureBean {
            /**
             * large : https://randomuser.me/api/portraits/men/13.jpg
             * medium : https://randomuser.me/api/portraits/med/men/13.jpg
             * thumbnail : https://randomuser.me/api/portraits/thumb/men/13.jpg
             */

            @SerializedName("large")
            private String large;
            @SerializedName("medium")
            private String medium;
            @SerializedName("thumbnail")
            private String thumbnail;

            public String getLarge() {
                return large;
            }

            public void setLarge(String large) {
                this.large = large;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }
        }


        public static final DiffUtil.ItemCallback<ResultsBean> DIFF_CALLBACK = new DiffUtil.ItemCallback<ResultsBean>() {

            @Override
            public boolean areItemsTheSame(ResultsBean oldItem, ResultsBean newItem) {
                return oldItem.getId().getValue().equals(newItem.getId().getValue());
            }

            @Override
            public boolean areContentsTheSame(ResultsBean oldItem, ResultsBean newItem) {
                return true;
            }
        };
    }
}
