package com.example.rajiv.shaadiproject.util.RecyclerviewDragDrop;

/**
 * Created by RajivLL on 27-Mar-18.
 */

public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();

}