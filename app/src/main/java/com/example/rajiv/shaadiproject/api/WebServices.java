package com.example.rajiv.shaadiproject.api;

import com.example.rajiv.shaadiproject.model.Matches;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebServices {

    int DEFAULT_PER_PAGE = 10;

    @GET("api/")
    Call<Matches> getMatches(@Query("results") String per_page);

}
