package com.example.rajiv.shaadiproject.util.RecyclerviewDragDrop;

import android.support.v7.widget.RecyclerView;

/**
 * Created by RajivLL on 27-Mar-18.
 */

public interface ItemTouchHelperAdapter {

    //boolean onItemMove(RecyclerView.ViewHolder fromHolder, RecyclerView.ViewHolder toHolder);

    void onItemDismiss(int position);
}
