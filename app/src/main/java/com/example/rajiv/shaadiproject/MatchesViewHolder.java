package com.example.rajiv.shaadiproject;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.rajiv.shaadiproject.model.Matches;


public class MatchesViewHolder extends RecyclerView.ViewHolder {

    View view;
    ImageView imageView;
    TextView nameTextView, ageTextView, locationTextView;
    FloatingActionButton deleteButton;

    private MatchesViewHolder(View itemView) {
        super(itemView);

        view = itemView;
        imageView = itemView.findViewById(R.id.imageView);
        nameTextView = itemView.findViewById(R.id.nameTextView);
        ageTextView = itemView.findViewById(R.id.ageTextView);
        locationTextView = itemView.findViewById(R.id.locationTextView);
        deleteButton = itemView.findViewById(R.id.deleteButton);

    }

    public static MatchesViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_matches, parent, false);
        return new MatchesViewHolder(view);
    }

    public void bindTo(Context context, Matches.ResultsBean resultsBean) {

        if (resultsBean != null) {


            Glide.with(context)
                    .load(resultsBean.getPicture().getLarge())
                    .apply(new RequestOptions()
                            .placeholder(R.mipmap.ic_launcher)
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(imageView);

            String name = resultsBean.getName().getFirst().substring(0, 1).toUpperCase() + resultsBean.getName().getFirst().substring(1) +
                    " " + (resultsBean.getName().getLast().toUpperCase().charAt(0));

            String location = resultsBean.getLocation().getCity().substring(0, 1).toUpperCase() + resultsBean.getLocation().getCity().substring(1) +
                    " , " + resultsBean.getLocation().getState().substring(0, 1).toUpperCase() + resultsBean.getLocation().getState().substring(1);


            nameTextView.setText(name);
            ageTextView.setText(String.valueOf(resultsBean.getDob().getAge()) + " yrs");
            locationTextView.setText(location);


            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        } else {

            nameTextView.setText("Loading...");
            ageTextView.setText("Loading...");
            locationTextView.setText("Loading...");

        }
    }
}
